from trino.dbapi import connect
import pandas as pd
if 'data_loader' not in globals():
    from mage_ai.data_preparation.decorators import data_loader
if 'test' not in globals():
    from mage_ai.data_preparation.decorators import test
 
@data_loader
def load_data(*args, **kwargs):

    """
    Template code for loading data from any source.
    Returns:
        Anything (e.g. data frame, dictionary, array, int, str, etc.)
    """
    # Specify your data loading logic here
    conn = connect(
    host="10.140.10.102",
    port=8080,
    user="malik",
    catalog="mssql",
    schema="",
    )

    cur = conn.cursor()
    cur.execute("SELECT nin, last_login_date, etl_inserted_at, etl_updated_at FROM mysql.db_test.jadarat_src order by last_login_date desc limit 100")
    rows = cur.fetchall()
    df = pd.DataFrame(rows,columns = ['nin', 'last_login_date', 'etl_inserted_at', 'etl_updated_at'])
    return df

 
@test
def test_output(output, *args) -> None:
    """
    Template code for testing the output of the block.
    """
    assert output is not None, 'The output is undefined'